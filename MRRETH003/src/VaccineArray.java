/**
* @author MRRETH003
*/
public class VaccineArray
{
    public int opCount;
    
    Vaccine[] data = new Vaccine[9919];
    int records = 0;
    
    /**
    * Allows for the addition of Vaccines to an array.
    * @param v Vaccine object.
    */
    public void add (Vaccine v)
    {
        data [records] = v;
        records++;
    }
    /**
    * Allows for a Vaccine to be searched for by comparing with the array of Vaccines.
    * @param v Vaccine object.
    */
    public Vaccine find (Vaccine v)
    {
        int count = 0;
        int i = 0;
        while (count < records)
        {
            int comp = v.compareTo (data [count]);
            opCount++;
            if (comp == 0)
            {
                return data [count];
            }
            count++;
        }
        return null;
    }
}