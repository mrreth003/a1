// Hussein's Binary Search Tree
// 26 March 2017
// Hussein Suleman

/**
* @author MRRETH003
*/
public class BinarySearchTree<Vaccine extends Comparable<? super Vaccine>> extends BinaryTree<Vaccine>
{
   /**
   * Insert Vaccine objects into Binary Tree.
   * @param v Vaccine object.
   */
   public void insert ( Vaccine v )
   {
      if (root == null)
         root = new BinaryTreeNode<Vaccine> (v, null, null);
      else
         insert (v, root);
   }
   
   /**
   * Recursive insert of Vaccine objects into binary tree to put node in the correct place.
   * @param v Vaccine object.
   * @param node Place where the Vaccine object is to be stored.
   */
   public void insert ( Vaccine v, BinaryTreeNode<Vaccine> node )
   {
      opCounter++;
      if (v.compareTo (node.data) <= 0)
      {
         if (node.left == null)
            node.left = new BinaryTreeNode<Vaccine> (v, null, null);
         else
            insert (v, node.left);
      }
      else
      {
         if (node.right == null)
            node.right = new BinaryTreeNode<Vaccine> (v, null, null);
         else
            insert (v, node.right);
      }
   }
   
   /**
   * Find a matching Vaccine object.
   * @param v Vaccine Object.
   * @return BinaryTreeNode <Vaccine> either null or recursion carry on searching.
   */
   public BinaryTreeNode<Vaccine> find ( Vaccine v )
   {
      if (root == null)
         return null;
      else
         return find (v, root);
   }
   
   /**
   *Recursive version of finding a matching Vaccine object.
   * @param v Vaccine object.
   * @param node Current node location on the BST.
   * @return BinaryTreeNode<Vaccine> either null, carry on searching or the correct node and therefore the correct vaccine.
   */
   public BinaryTreeNode<Vaccine> find ( Vaccine v, BinaryTreeNode<Vaccine> node )
   {
      opCount++;
      if (v.compareTo (node.data) == 0) 
         return node;
      else if (v.compareTo (node.data) < 0)
      {
         
         opCount++;
         return (node.left == null) ? null : find (v, node.left);
      }
      else
      {
         opCount++;
         return (node.right == null) ? null : find (v, node.right);
      }
   }
   
   /**
   * Remove a Vaccine object from the BST.
   * @param v Vaccine object.
   */
   public void delete ( Vaccine v )
   {
      root = delete (v, root);
   }   
   
   /**
   * Recursively search for and delete a Vaccine object from the BST.
   * @param v Vaccine object.
   * @param node Current location checking.
   * @return BinaryTreeNode<Vaccine> either null or node where deletion took place.
   */
   public BinaryTreeNode<Vaccine> delete ( Vaccine v, BinaryTreeNode<Vaccine> node )
   {
      opCount++;
      if (node == null) return null;
      if (v.compareTo (node.data) < 0)
         node.left = delete (v, node.left);
      else if (v.compareTo (node.data) > 0)
      {
         opCount++;
         node.right = delete (v, node.right);
      }
      else if (node.left != null && node.right != null )
      {
         opCount += 2;
         node.data = findMin (node.right).data;
         node.right = removeMin (node.right);
      }
      else
         if (node.left != null)
            node = node.left;
         else
            node = node.right;
      return node;
   }
   
   /**
   * Finds the minimum value in the BST.
   * @param node Current Location checking.
   * @return BinaryTreeNode<Vaccine> node location on BST of min value.
   */
   public BinaryTreeNode<Vaccine> findMin ( BinaryTreeNode<Vaccine> node )
   {
      if (node != null)
         while (node.left != null)
         {
            node = node.left;
         }
      return node;
   }

   /**
   * Finds and removes the minimum value in the BST.
   * @param node Current Location checking.
   * @return BinaryTreeNode<Vaccine> node location on BST of min value removed or null.
   */
   public BinaryTreeNode<Vaccine> removeMin ( BinaryTreeNode<Vaccine> node )
   {
      if (node == null)
         return null;
      else if (node.left != null)
      {
         node.left = removeMin (node.left);
         return node;
      }
      else
         return node.right;
   }
   
}
