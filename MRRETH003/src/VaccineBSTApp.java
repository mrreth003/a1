import java.io.*;
import java.util.*;
/**
* @author MRRETH003
*/
public class VaccineBSTApp
{
    BinarySearchTree<Vaccine> bst = new BinarySearchTree<Vaccine> ();
    
    /**
    * Read data into BinarySearchTree from .csv file containing lines for Vaccine objects.
    * @exception e Prevents an error when reading from file.
    */
    public void readInFile ()
    {
        // read content of file into data structure 
        File f = new File ("data/vaccinations.csv");
        try
        {
          Scanner sc = new Scanner (f);
          while (sc.hasNextLine ())
          {
              String line = sc.nextLine ();
              bst.insert (new Vaccine(line));
          }
        }
        catch (Exception e)
        {
          //do something, report an error
          System.out.println ("An error occurred when reading the file");
        }
    }

    /**
    * Interact with the user and process inputted data.
    */
    public void userInterface ()
    {
       Scanner sc = new Scanner (System.in);
       System.out.println ("Enter the date:");
       String d = sc.nextLine ();
       System.out.println ("Enter the list of countries (end with an empty line):");
       int count = 0;
       String [] c = new String [1];
       c [count] = sc.nextLine ();
       do
       {
         count++;
         c = Arrays.copyOf (c, c.length + 1);
         c[count] = sc.nextLine ();
       } while ((!(c [count].equals (""))));
               
       System.out.println ("Results:");
       for (int i = 0; i < count; i++)
       {
         Vaccine v = new Vaccine (c [i] + "," + d + ",");
         BinarySearchTree<Vaccine> btn = new BinarySearchTree<Vaccine> ();
         btn.root = bst.find (v);        
         if (btn.root == null)
         {
           System.out.println (c [i] + " = <Not Found>");
         }
         else
         {
           System.out.println (c [i] + " = " + btn.root.data.vaccinations);
         }
       }
       System.out.println("opCount = " + bst.opCount);
    }

    /**
    * Main method which is run first and calls any other methods required by the program to run.
    * @param args An array fo strings containing any potential arguments specified by the user when running the program.
    */
    public static void main (String [] args)
    {
      VaccineBSTApp vba = new VaccineBSTApp ();
      vba.readInFile ();
      vba.userInterface ();
    }
}


