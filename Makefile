# Makefile for A1
# Ethan Morris
# 26/02/22

.SUFFIXES: .java .class
SRCDIR=src
BINDIR=bin
JAVAC=/usr/bin/javac
JAVA=/usr/bin/java

$(BINDIR)/%.class: $(SRCDIR)/%.java
	$(JAVAC) -d $(BINDIR)/ -cp $(BINDIR) $<

CLASSES=Vaccine.class \
	VaccineArray.class \
	VaccineArrayApp.class \
	BinaryTreeNode.class \
	BTQueueNode.class \
        BTQueue.class \
        BinaryTree.class \
	BinarySearchTree.class \
	BinarySearchTreeTest.class \
	VaccineBSTApp.class \
#	Experiment.class
	
CLASS_FILE=$(CLASSES:%.class=$(BINDIR)/%.class)

default: $(CLASS_FILE)

run: $(CLASS_FILE)
	$(JAVA) -cp $(BINDIR) VaccineArrayApp
	$(JAVA) -cp $(BINDIR) VaccineBSTApp
#	$(JAVA) -cp $(BINDIR) Experiment

clean:
	rm $(BINDIR)/*.class