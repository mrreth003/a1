import java.io.*;
import java.util.*;

/**
* @author MRRETH003
*/
public class VaccineArrayApp
{
    /**
    * Creates new VaccineArray object in order for the input file to store its data.
    */
    VaccineArray va = new VaccineArray ();
    
    /**
    * Read data into VaccineArray from .csv file containing lines for Vaccine objects.
    * @exception e Prevents error when reading in a .csv file.
    */
    public void readInFile ()
    {
        // read content of file into data structure 
        File f = new File ("data/vaccinations.csv");
        try
        {
          Scanner sc = new Scanner (f);
          while (sc.hasNextLine ())
          {
              String line = sc.nextLine ();
              va.add (new Vaccine(line));
          }
        }
        catch (Exception e)
        {
          //do something, report an error
          System.out.println ("An error occurred when reading the file");
        }
    }
    
    /**
    * Interact with the user and process inputted data.
    */
    public void userInterface ()
    {
        // interactive interface
        Scanner sc = new Scanner (System.in);
        System.out.println ("Enter the date:");
        String d = sc.nextLine ();
        System.out.println ("Enter the list of countries (end with an empty line):");
        int count = 0;
        String [] c = new String [1];
        c [count] = sc.nextLine ();
        do
        {
          count++;
          c = Arrays.copyOf (c, c.length + 1);
          c[count] = sc.nextLine ();
        } while ((!(c [count].equals (""))));
                
        System.out.println ("Results:");
        for (int i = 0; i < count; i++)
        {
          Vaccine v = new Vaccine (c [i] + "," + d + ",");
          v = va.find (v);        
          if (v == null)
          {
            System.out.println (c [i] + " = <Not Found>");
          }
          else
          {
            System.out.println (c [i] + " = " + v.vaccinations);
          }
        }
        System.out.println("opCount = " + va.opCount);
    }
    
    /**
    * Main method which is run first and calls any other methods required by the program to run.
    * @param args An array fo strings containing any potential arguments specified by the user when running the program.
    */
    public static void main (String [] args)
    {
      VaccineArrayApp vaa = new VaccineArrayApp ();
      vaa.readInFile ();
      vaa.userInterface ();
    }
}