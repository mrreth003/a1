import java.util.*;
import java.io.*;

/**
* @author MRRETH003
*/
public class Experiment
{
    /**
    * Main function & only function which runs in class.
    * Checks input file values against another file of input values.
    * @param args Array of String arguments that could have been specified by the user when running the class.
    * @exception e Prevents an error from occuring when in the data from both files.
    */
    public static void main (String[] args)
    {
        BinarySearchTree<Vaccine> bst = new BinarySearchTree<Vaccine> ();
        
         // read content of file into data structure 
        File f = new File ("data/n10.txt");
        try
        {
          Scanner sc = new Scanner (f);
          while (sc.hasNextLine ())
          {
              String line = sc.nextLine ();
              bst.insert (new Vaccine(line));              
          }
        }
        catch (Exception e)
        {
          //do something, report an error
          System.out.println ("An error occurred when reading the file");
        }
        f = new File ("data/n10.txt");
        try
        {        
          Scanner sc = new Scanner (f);
          while (sc.hasNextLine ())
          {
              String line = sc.nextLine ();
              String c = new String();
              String d = new String();

              int i = line.indexOf(',');
              c = line.substring(0, i);
              d = line.substring(i + 1);
              i = d.indexOf(',');
              d = d.substring(0, i);

              Vaccine v = new Vaccine( c + "," + d + ",");
              BinarySearchTree<Vaccine> btn = new BinarySearchTree<Vaccine> ();
              btn.root = bst.find (v);
              if (bst.opCount > bst.opMax)
              {
                  bst.opMax = bst.opCount;
              }
              bst.opSum += bst.opCount;
              bst.opCount = 0;
          }
          System.out.println("opMax = " + bst.opMax);
          System.out.println("opSum = " + bst.opSum);
        }
        catch (Exception e)
        {
          //do something, report an error
          System.out.println ("An error occurred when reading the file");
        }

    }
}
